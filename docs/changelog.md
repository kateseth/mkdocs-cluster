# Changelog

## v0.0.7 – 18 February 2017

 * Fixed crash when page has no heading (Closes #1)

## v0.0.6 – 25 November 2016

 * Add missing js Highlight init

## v0.0.5 – 24 November 2016

 * Fixed badly formated ul tag in toc template

## v0.0.4 – 18 November 2016

 * Update templates to work with mkdocs v0.16.0

## v0.0.3 – 02 September 2016

 * Optional logo from Font Awesome
 * Add a 404 page
 * Fetch external css, js and font on build

## v0.0.2 – 21 Jun 2016

  * Add missing font file

## v0.0.1 – 21 Jun 2016

  * Initial release
