#!/bin/sh

ROOT=$(dirname $(readlink -f $0))
SCRIPT=$(basename $0)

_inspect () {
    # Auto generate help string
    local help=$(awk '$1 ~ /^[a-z]+_?[a-z]+$/ && $2 == "()" { printf "%s|", $1 }' $0)
    echo ${help%|}
}

#-----------------------------------------------------------------------------#

_is_exe () {
    command -v $1 >/dev/null 2>&1 || { echo >&2 "missing $1 command"; return 1; }
    return 0
}

if [ $# -eq 0 ]
then
    echo "${ROOT}/${SCRIPT} $(_inspect)"
    exit
fi


FA="https://github.com/FortAwesome/Font-Awesome/archive/v4.6.3.tar.gz"
BSTP_CSS="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
BSTP_JS="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
BSTP_FONT0="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/fonts/glyphicons-halflings-regular.eot"
BSTP_FONT1="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/fonts/glyphicons-halflings-regular.svg"
BSTP_FONT2="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/fonts/glyphicons-halflings-regular.ttf"
BSTP_FONT3="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/fonts/glyphicons-halflings-regular.woff"
BSTP_FONT4="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/fonts/glyphicons-halflings-regular.woff2"

JQUERY="https://code.jquery.com/jquery-1.12.4.min.js"
HLIGHT_JS="http://cdn.jsdelivr.net/highlight.js/9.4.0/highlight.min.js"
HLIGHT_CSS="http://cdn.jsdelivr.net/highlight.js/9.4.0/styles/default.min.css"
#HLIGHT_CSS="http://cdn.jsdelivr.net/highlight.js/9.4.0/styles/solarized-light.min.css"
#HLIGHT_CSS="http://cdn.jsdelivr.net/highlight.js/9.4.0/styles/solarized-dark.min.css"

clean () {
    git clean -fdx ${ROOT}/mkdocs_cluster/
}

get_libs () {
    printf "Downloading/extracting third libraries: "
    wget -q -O ${ROOT}/mkdocs_cluster/css/bootstrap.min.css "$BSTP_CSS" && printf "o" || printf "x"
    wget -q -O ${ROOT}/mkdocs_cluster/js/bootstrap.min.js  "$BSTP_JS"   && printf "o" || printf "x"
    for i in 0 1 2 3 4;do
        local resource=$(eval echo \$$(eval echo BSTP_FONT${i}))
        wget -q --directory-prefix=${ROOT}/mkdocs_cluster/fonts/ "${resource}" && printf "o" || printf "x"
    done
    wget -q -O ${ROOT}/mkdocs_cluster/js/jquery.min.js "$JQUERY"        && printf "o" || printf "x"
    wget -q -O ${ROOT}/mkdocs_cluster/js/highlight.min.js "$HLIGHT_JS"  && printf "o" || printf "x"
    wget -q -O ${ROOT}/mkdocs_cluster/css/highlight.min.css "$HLIGHT_CSS"&& printf "o" || printf "x"
    wget -q -O _fa.tar.gz ${FA} && printf "o" || printf "x"
    tar -C ${ROOT}/mkdocs_cluster/ --strip-components=1 -xzf _fa.tar.gz Font-Awesome-4.6.3/fonts/
    tar -C ${ROOT}/mkdocs_cluster/css --strip-components=2 -xzf _fa.tar.gz Font-Awesome-4.6.3/css/font-awesome.min.css
    printf " Done\n"
    rm _fa.tar.gz
    shacheck
}
shacheck () {
    printf "Controling files integrity: "
    echo "# Control sums
    288808946fa1a358cb8f45b53c752e3fc1726eca30d859ef2c0cd287f86700e5  mkdocs_cluster/css/base.css
    eece6e0c65b7007ab0eb1b4998d36dafe381449525824349128efc3f86f4c91c  mkdocs_cluster/css/bootstrap.min.css
    008a1d103902f15fdb1c191fcb1ce8954330e7b8de43d09abb08555ba609f420  mkdocs_cluster/css/font-awesome.min.css
    65dd6271f67bd94066b0877f99471a82b98dec8379424aef87be480872105539  mkdocs_cluster/css/highlight.min.css
    9c50c88a50f3e73070486a04aaad4c48334baa2ae2bd329158a0a4fb5d4f585e  mkdocs_cluster/js/base.js
    2979f9a6e32fc42c3e7406339ee9fe76b31d1b52059776a02b4a7fa6a4fd280a  mkdocs_cluster/js/bootstrap.min.js
    a65809bbb3a6c44eca3cdcee0d7152d2373b5644c6f079617427724e927b83be  mkdocs_cluster/js/highlight.min.js
    668b046d12db350ccba6728890476b3efee53b2f42dbb84743e5e9f1ae0cc404  mkdocs_cluster/js/jquery.min.js
    13634da87d9e23f8c3ed9108ce1724d183a39ad072e73e1b3d8cbf646d2d0407  mkdocs_cluster/fonts/glyphicons-halflings-regular.eot
    42f60659d265c1a3c30f9fa42abcbb56bd4a53af4d83d316d6dd7a36903c43e5  mkdocs_cluster/fonts/glyphicons-halflings-regular.svg
    e395044093757d82afcb138957d06a1ea9361bdcf0b442d06a18a8051af57456  mkdocs_cluster/fonts/glyphicons-halflings-regular.ttf
    a26394f7ede100ca118eff2eda08596275a9839b959c226e15439557a5a80742  mkdocs_cluster/fonts/glyphicons-halflings-regular.woff
    fe185d11a49676890d47bb783312a0cda5a44c4039214094e7957b4c040ef11c  mkdocs_cluster/fonts/glyphicons-halflings-regular.woff2
    ecd72f31910a8ee2726fd17bd459be26f230779f3f3ed5f69ebf829e4b12e768  mkdocs_cluster/fonts/FontAwesome.otf
    50bbe9192697e791e2ee4ef73917aeb1b03e727dff08a1fc8d74f00e4aa812e1  mkdocs_cluster/fonts/fontawesome-webfont.eot
    8e3586389bb4cd01b3f85bb3b622739bde6627f28bba63a020c223ca9cf1b9ae  mkdocs_cluster/fonts/fontawesome-webfont.svg
    ae19e2e4c04f2b04bf030684c4c1db8faf5c8fe3ee03d1e0c409046608b38912  mkdocs_cluster/fonts/fontawesome-webfont.ttf
    adbc4f95eb6d7f2738959cf0ecbc374672fce47e856050a8e9791f457623ac2c  mkdocs_cluster/fonts/fontawesome-webfont.woff
    7dacf83f51179de8d7980a513e67ab3a08f2c6272bb5946df8fd77c0d1763b73  mkdocs_cluster/fonts/fontawesome-webfont.woff2" | \
    sha256sum -c --quiet - && printf "Done\n"
}

build_py () {
    rm -rf ${ROOT}/dist
    python3 setup.py bdist_wheel --universal
    python3 setup.py sdist
}

pypi_pub () {
    twine upload dist/*.tar.gz dist/*.whl
}

build () {
    _is_exe mkdocs || exit 1
    rm -rf dist/site.zip
    mkdocs build
    echo "Zipping site file"
    (cd site && zip -qr ${ROOT}/dist/site ./*)
}

$@

# vim: fileencoding=utf8 ft=sh
